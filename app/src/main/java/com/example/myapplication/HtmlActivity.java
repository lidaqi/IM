package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import com.qiniu.android.storage.Configuration;
import com.qiniu.android.storage.UploadManager;

public class HtmlActivity extends BaseActivity {
    private WebView webView;

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button:
                    // 无参数调用 JS的方法
                    webView.loadUrl("javascript:javaToJS()");
                    break;
                case R.id.button2:
                    // 传递参数调用JS的方法，点击后调用document.location=arg切换网页
                    webView.loadUrl("javascript:javaToJsWith(" + "'https://baike.so.com/doc/456230-483111.html'" + ")");
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_html);

        webView = findViewById(R.id.webview);
        // 启用javascript
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        webView.setWebChromeClient(new WebChromeClient());
        // 从assets目录下面的加载html
        webView.loadUrl("file:///android_asset/im.html");
        //name:android在网页里面可以用window.name.方法名调用java方法
        webView.addJavascriptInterface(HtmlActivity.this, "java");

        //Button1按钮 无参调用HTML js方法
        Button button = findViewById(R.id.button);
        button.setOnClickListener(mOnClickListener);
        //Button2按钮 有参调用HTML js方法
        Button button2 = findViewById(R.id.button2);
        button2.setOnClickListener(mOnClickListener);

//        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // 无参数调用 JS的方法
//                webView.loadUrl("javascript:javaToJS()");
//
//            }
//        });
        //Button2按钮 有参调用HTML js方法
//        findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                // 传递参数调用JS的方法，点击后调用document.location=arg切换网页
//                webView.loadUrl("javascript:javaToJsWith(" + "'https://baike.so.com/doc/456230-483111.html'" + ")");
//            }
//        });

    }
}