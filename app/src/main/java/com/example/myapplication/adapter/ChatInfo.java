package com.example.myapplication.adapter;

import java.io.Serializable;

public class ChatInfo implements Serializable {

    private String mMsg;
    private int mType;
    private int mDataType;
    private String mPath = null;

    public static int self = 1;
    public static int other = 0;

    public static int text = 0;
    public static int record = 1;
    public static int image = 2;

    public ChatInfo(String msg) {
        mMsg = msg;
        mType = other;
        mDataType = text;
    }

    public ChatInfo(String msg, int type, int dataType, String path) {
        mMsg = msg;
        mType = type;
        mDataType = dataType;
        mPath = path;
    }

    public String getMsg() {
        return mMsg;
    }

    public String getPath() {
        return mPath;
    }

    public boolean isText() {
        return mDataType == text;
    }

    public boolean isRecord() {
        return mDataType == record;
    }

    public boolean isImage() {
        return mDataType == image;
    }

    public boolean isSelf() {
        return mType == self;
    }

    public boolean isOther() {
        return mType == other;
    }

}
