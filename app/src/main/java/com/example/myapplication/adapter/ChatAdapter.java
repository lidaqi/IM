package com.example.myapplication.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myapplication.BaseActivity;
import com.example.myapplication.R;
import com.qiniu.android.http.HttpEventListener;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ChatAdapter extends BaseAdapter {

    private static final String TAG = "testlog";
    private Context mContext;
    private List<ChatInfo> mData;

    private LayoutInflater mLayoutInflater;

    public ChatAdapter(Context context, List<ChatInfo> data) {
        mContext = context;
//        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mLayoutInflater = LayoutInflater.from(context);
        mData = data;
    }

    @Override
    public int getCount() {
        // 多少条数据
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        // 返回某一条数据
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        // 返回某一条数据的ID
        return position;
    }

    public ImageView item = null;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // 返回一个视图
        convertView = mLayoutInflater.inflate(R.layout.item_chat, null);

        ChatInfo data = mData.get(position);
        // 获取控件
        if (data.isImage()) {
            // 图片消息

            // 标题控件
            settingItem(convertView, position);

            // 图片控件
            item = convertView.findViewById(R.id.item_image_view);
            // 设置控件可见
            item.setVisibility(item.VISIBLE);
            // 设置图片
            item.setImageURI(Uri.fromFile(new File(data.getPath())));

        } else {
            // 文本消息
            settingItem(convertView, position);
        }

        return convertView;
    }

    public void settingItem(View convertView, int position) {
        TextView item = convertView.findViewById(R.id.item_text_view);
        if (mData.get(position).isOther()) {
            item.setGravity(Gravity.LEFT);
        } else {
            item.setGravity(Gravity.RIGHT);
        }
        // 设置控件可见
        item.setVisibility(item.VISIBLE);
        // 绑定数据
        item.setText(mData.get(position).getMsg());
    }
}
